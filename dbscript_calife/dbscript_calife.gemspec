# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'dbscript_calife/version'

Gem::Specification.new do |spec|
  spec.name          = "dbscript_calife"
  spec.version       = DbscriptCalife::VERSION
  spec.authors       = ["calife"]
  spec.email         = ["califerno@gmail.com"]

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] ="http://gems.efmnet.it/"
  end

  spec.summary       = %q{Database Script creation tool}
  spec.description   = %q{Database Script creation tool for Oracle, Archibus FM application}
  spec.homepage      = "https://bitbucket.org/calife/dbscript_calife"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"

  spec.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }

  
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "geminabox"
end
