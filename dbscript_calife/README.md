# DbscriptCalife

Database Script creation tool for Oracle, Archibus FM application

## Installation

gem install dbscript_calife

```ruby
gem 'dbscript_calife'
```

And then execute:

    $ bundle exec bin/dbscript_calife

Or install it yourself as:

    $ gem install dbscript_calife

## Usage

```ruby
gem 'dbscript_calife'

# Read database parameters con command line, generate scripts into /tmp/database_name
parameters=DbscriptCalife::Cli.start
DbscriptCalife::OracleGenerator.new(parameters).generate
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release` to create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

1. Fork it ( https://github.com/calife/dbscript_calife/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
