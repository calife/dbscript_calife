require "dbscript_calife/version"
require 'tmpdir'

module DbscriptCalife

  def self.root
    File.dirname __dir__
  end
  
  def self.lib
    File.join root, 'lib'
  end

  def self.TEMPLATE
    File.join lib, 'TEMPLATE'
  end
  
  class Helper

    def self.error(msg)
      puts "#{msg}..."
      exit 1
    end

    def self.askConfirm(msg)
      
      while true
        printf "#{msg}"
        answer=STDIN.gets.chomp
        return true  if answer =~ /^y/i
        return false if answer =~ /^n/i

      end
      
    end

    def self.to_megabyte(data)
      if _is_mega?(data)
        return data
      end
      if _is_giga?(data)
        data=  (((data.to_f*1024)  . round) . to_s) +"M"
        return data
      end
      if _is_numeric?(data)
        data=  (data.to_s) +"M"
        return data
      end
      
    end

private
    def self._is_mega?(argument)
      if  argument =~ /^\d*\.?\d*MB?$/i
        true
      else
        false
      end
    end  

    def self._is_giga?(argument)
      if  argument =~ /^\d*\.?\d*GB?$/i
        true
      else
        false
      end
    end  

    def self._is_numeric?(argument)
      if argument.to_s =~ /^\d*(\.\d+)?$/
        true
      else
        false      
      end
    end    

  end


  class Consts
    DEFAULT_NAME="new_instance"
    DEFAULT_ORADATA_DIR="/opt/oracle/oradata"
    DEFAULT_AFM_PASSWORD="SOME_AFM_PASSWD"
    DEFAULT_AFM_P1_SIZE="512M"
    DEFAULT_AFM_DOC_MGMT_SIZE="512M"
    DEFAULT_MAX_DATAFILE_SIZE="27648M" # Max datafile size , default to 27GB
  end

  class Cli
    def self.start()
      parameters={}
      
      print "Database name [default to #{DbscriptCalife::Consts::DEFAULT_NAME}]: "
      name=STDIN.gets.chomp
      name = DbscriptCalife::Consts::DEFAULT_NAME  if name.empty?
      parameters[:name]= name      

      print "Directory [default to #{DbscriptCalife::Consts::DEFAULT_ORADATA_DIR}]: "
      oradata_dir=STDIN.gets.chomp
      oradata_dir = DbscriptCalife::Consts::DEFAULT_ORADATA_DIR  if oradata_dir.empty?
      parameters[:oradata_dir]= oradata_dir

      print "Password [default to #{DbscriptCalife::Consts::DEFAULT_AFM_PASSWORD}]: "
      password=STDIN.gets.chomp
      password = DbscriptCalife::Consts::DEFAULT_AFM_PASSWORD  if password.empty?
      parameters[:password]= password

      print "Datafile AFM_P1 size: [default to #{DbscriptCalife::Consts::DEFAULT_AFM_P1_SIZE}]: "
      afm_p1_size=STDIN.gets.chomp
      afm_p1_size = DbscriptCalife::Consts::DEFAULT_AFM_P1_SIZE  if afm_p1_size.empty?
      parameters[:afm_p1_size]= DbscriptCalife::Helper.to_megabyte( afm_p1_size )
      
      print "Datafile AFM_DOC_MGMT size: [default to #{DbscriptCalife::Consts::DEFAULT_AFM_DOC_MGMT_SIZE}]: "
      afm_doc_mgmt_size=STDIN.gets.chomp
      afm_doc_mgmt_size = DbscriptCalife::Consts::DEFAULT_AFM_DOC_MGMT_SIZE  if afm_doc_mgmt_size.empty?
      parameters[:afm_doc_mgmt_size]= DbscriptCalife::Helper.to_megabyte ( afm_doc_mgmt_size )

      puts "Data: #{DbscriptCalife::Helper.to_megabyte ( afm_doc_mgmt_size )}"

      return parameters
      
    end
  end
  
  class OracleGenerator

=begin    
       Initialize or load default settings
=end
    def initialize(parameters)

      @parameters=parameters

      @name=@parameters[:name] #database name
      @oradata_dir=@parameters[:oradata_dir].chomp('/') # remove trailing '/' # oracle oradata directory
      @afm_password=@parameters[:password] # AFM/AFM_SECURE user password

      @afm_p1_size=@parameters[:afm_p1_size] # AFM_P1 data size
      @afm_doc_mgmt_size=@parameters[:afm_doc_mgmt_size] # AFM_DOC_MGMT data size

      @afm_p1_num_datafiles=_num_of_datafiles(@afm_p1_size) # number of AFM_P1 datafiles (computed)
      @afm_doc_mgmt_num_datafiles=_num_of_datafiles(@afm_doc_mgmt_size)  # number of AFM_DOC_MGMT datafiles (computed)

      @template=DbscriptCalife.TEMPLATE
      @output=@parameters[:output] || Dir.tmpdir() # Destination folder
    end

    def generate()

      _genera_script_base
      _genera_script_cambio_password
      _genera_script_tablespace_AFM_P1
      _genera_script_tablespace_AFM_BLOB

      printf "Scripting files generated into #{@output}/#{@name} \n "
      
    end

    def printConfiguration
      printf "%s%s%s\n" , "#" * 20 ,  %Q{ =Start configuration= } , "#" * 20
      printf "%s\n" , %Q{ Database name: #{@name} }
      printf "%s\n" , %Q{ Database dir : #{@oradata_dir} }
      printf "%s\n" , %Q{ AFM password : #{@afm_password} }
      printf "%s\n" , %Q{ AFM_P1 maxsize  : #{@afm_p1_size} }
      printf "%s\n" , %Q{ AFM_P1 datafiles: #{@afm_p1_num_datafiles} }      
      printf "%s\n" , %Q{ AFM_DOC maxsize : #{@afm_doc_mgmt_size} }
      printf "%s\n" , %Q{ AFM_DOC datafiles: #{@afm_doc_mgmt_num_datafiles} }
      printf "%s%s%s\n" , "#" * 20 ,  %Q{ =End configuration= } , "#" * 20
    end

    private
    def _mkdir_output_folder

      scriptfiles = File.join(File.join(@output,@name),"*.{sql,sh}")
      Dir.glob(scriptfiles).each { |filen|
        File.delete(filen)
      }
      Dir.delete(File.join(@output,@name)) if Dir.exists?(File.join(@output,@name))
      Dir.mkdir(File.join(@output,@name))
      
    end
    
    def _genera_script_base
      
      # mkdir output folder
      _mkdir_output_folder
      
      # Copy the basic template
      system %Q{ cp -a #{@template}/*  #{@output}/#{@name} }

      # sed in file TEMPLATE => @name
      system %Q{ find #{@output}/#{@name} -maxdepth 1 -type f -a \\( -name '*.sql' -o -name '*.sh' \\) | xargs sed -i "s/TEMPLATE/#{@name}/g" ; }

      # sed in file pattern DEFAULT_ORADATA_PARAM  => @oradata_dir
      system %Q{ find #{@output}/#{@name} -maxdepth 1 -type f -a \\( -name '*.sql' -o -name '*.sh' \\) | xargs sed -i "s/DEFAULT_ORADATA_PARAM/#{@oradata_dir.gsub('/','\/')}/g" ; }

      # File rename , replace  pattern TEMPLATE with @name
      Dir.glob("#{@output}/#{@name}/*TEMPLATE*").sort.each do |f|
        File.rename f , f.gsub('TEMPLATE',@name)
      end
      
    end

    def _genera_script_cambio_password
      
      # sed in file pwd.sql, pattern AFM_PASSWORD => @afm_password
      system %Q{ find #{@output}/#{@name} -maxdepth 1 -type f -name "pwd.sql" | xargs sed -i "s/AFM_PASSWD/#{@afm_password}/g" ; }
      
    end

    def _genera_script_tablespace_AFM_P1
      
      #  Replace DEFAULT_AFM_P1_SIZE_PARAM with #{DbscriptCalife::Consts::DEFAULT_AFM_P1_SIZE} into *.sql files
      system %Q{ find #{@output}/#{@name} -maxdepth 1 -type f -name "*.sql" | xargs sed -i "s/DEFAULT_AFM_P1_SIZE_PARAM/#{DbscriptCalife::Consts::DEFAULT_AFM_P1_SIZE}/g"; \n }

      # Scrive il file tblspace.sql
      str=""
      str << %Q{SPOOL TBLSPACE.LST \n}
      
      (1..@afm_p1_num_datafiles).each { |counter|
        if counter==1
          if @afm_p1_size.to_i < DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE.to_i
            str << %Q{ALTER DATABASE DATAFILE '#{@oradata_dir}/#{@name}/AFM_P1.dbf' AUTOEXTEND ON NEXT 500M MAXSIZE #{@afm_p1_size}; \n}
          else
            str << %Q{ALTER DATABASE DATAFILE '#{@oradata_dir}/#{@name}/AFM_P1.dbf' AUTOEXTEND ON NEXT 500M MAXSIZE #{DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE}; \n}
          end
        else
          if counter == @afm_p1_num_datafiles # last datafile

            last_size=(@afm_p1_size.to_i  % DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE.to_i )
            
            str << %Q{ALTER TABLESPACE "AFM_P1" ADD DATAFILE '#{@oradata_dir}/#{@name}/AFM_P#{counter}.dbf' SIZE  #{last_size}M  AUTOEXTEND ON NEXT 500M MAXSIZE #{last_size}M; \n\n}
          else
            str << %Q{ALTER TABLESPACE "AFM_P1" ADD DATAFILE '#{@oradata_dir}/#{@name}/AFM_P#{counter}.dbf' SIZE  #{DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE}  AUTOEXTEND ON NEXT 500M MAXSIZE  #{DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE}; \n\n}
          end
          
        end
      }
      
      File.open( "#{@output}/#{@name}/tblspace.sql"  , "a") { |file| file << str }
      
    end

    def _genera_script_tablespace_AFM_BLOB

      #  Replace DEFAULT_AFM_DOC_MGMT_SIZE_PARAM with @afm_doc_mgmt_size into  *.sql files
      system %Q{ find #{@output}/#{@name} -maxdepth 1 -type f -name "*.sql" | xargs sed -i "s/DEFAULT_AFM_DOC_MGMT_SIZE_PARAM/#{DbscriptCalife::Consts::DEFAULT_AFM_DOC_MGMT_SIZE}/g"; \n }
      
      str=""
      (1..@afm_doc_mgmt_num_datafiles).each { |counter|
        if counter==1
          if @afm_doc_mgmt_size.to_i < DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE.to_i
            str << %Q{ALTER DATABASE DATAFILE '#{@oradata_dir}/#{@name}/AFMDOCMGMT_BLOB1.dbf' AUTOEXTEND ON NEXT 500M MAXSIZE #{@afm_doc_mgmt_size}; \n}
          else
            str << %Q{ALTER DATABASE DATAFILE '#{@oradata_dir}/#{@name}/AFMDOCMGMT_BLOB1.dbf' AUTOEXTEND ON NEXT 500M MAXSIZE #{DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE}; \n}
          end
        else
          if counter == @afm_doc_mgmt_num_datafiles # last datafile

            last_size= ( @afm_doc_mgmt_size.to_i  % DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE.to_i )
            
            str << %Q{ALTER TABLESPACE "AFMDOCMGMT_BLOB" ADD DATAFILE '#{@oradata_dir}/#{@name}/AFMDOCMGMT_BLOB#{counter}.dbf' SIZE  #{last_size}M  AUTOEXTEND ON NEXT 500M MAXSIZE #{last_size}M; \n\n}
          else
            str << %Q{ALTER TABLESPACE "AFMDOCMGMT_BLOB" ADD DATAFILE '#{@oradata_dir}/#{@name}/AFMDOCMGMT_BLOB#{counter}.dbf' SIZE  #{DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE}  AUTOEXTEND ON NEXT 500M MAXSIZE  #{DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE}; \n\n}
          end
        end
      }

      str << %Q{SPOOL OFF \n}
      str << %Q{EXIT \n}

      File.open( "#{@output}/#{@name}/tblspace.sql"  , "a") { |file| file << str }

    end
    
    # Compute the number of datafiles required to store size (MB) of data, result depends on DEFAULT_MAX_DATAFILE_SIZE set by default to 27GB
    def _num_of_datafiles(size_required)
      if size_required.to_i < DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE.to_i
        return 1
      else
        return ( size_required.to_i / DbscriptCalife::Consts::DEFAULT_MAX_DATAFILE_SIZE.to_i ).next
      end                                                                        
    end

  end

  
end
